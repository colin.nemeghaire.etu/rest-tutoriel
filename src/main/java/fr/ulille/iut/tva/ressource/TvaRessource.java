package fr.ulille.iut.tva.ressource;

import java.util.ArrayList;
import java.util.List;

import fr.ulille.iut.tva.dto.InfoTauxDto;
import fr.ulille.iut.tva.service.CalculTva;
import fr.ulille.iut.tva.service.TauxTva;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;

/**
 * TvaRessource
 */

@Path("tva")
public class TvaRessource {
	private CalculTva calculTva = new CalculTva();



	@GET
	@Path("tauxpardefaut")
	public double getValeurTauxParDefaut() {
		return TauxTva.NORMAL.taux;
	}

	@GET
	@Path("valeur/{niveauTva}")
	public double getValeurTaux(@PathParam("niveauTva") String niveau) {
		try {
			return TauxTva.valueOf(niveau.toUpperCase()).taux; 
		}
		catch ( Exception ex ) {
			throw new NiveauTvaInexistantException();
		}

	}

	@GET
	@Path("{niveauTva}")
	public double getMontantTotal(@PathParam("niveauTva") String niveau,@QueryParam("somme") int somme) {
		try {
			return (TauxTva.valueOf(niveau.toUpperCase()).taux*somme)/100;
		}
		catch ( Exception ex ) {
			throw new NiveauTvaInexistantException();
		}
	}
	
	@GET
	@Path("lestaux")
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public List<InfoTauxDto> getInfoTaux() {
	  ArrayList<InfoTauxDto> result = new ArrayList<InfoTauxDto>();
	  for ( TauxTva t : TauxTva.values() ) {
	    result.add(new InfoTauxDto(t.name(), t.taux));
	  }
	  return result;
	}

	@GET
	@Path("details/{taux}")
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public String getDetail(@PathParam("taux") String niveau,@QueryParam("somme") double somme) {
	  InfoTauxDto inftauc = new InfoTauxDto(TauxTva.valueOf(niveau.toUpperCase()).name(), TauxTva.valueOf(niveau.toUpperCase()).taux);
	  
	  return "{ \"montantTotal\" "+(somme+(somme*inftauc.getTaux())/100)+", \"montantTva\": "+((somme*inftauc.getTaux())/100)+", \"somme\" "+somme+", \"tauxLabel\": \""+inftauc.getLabel()+"\", \"tauxValue\" "+inftauc.getTaux()+"}\n";
	}


}
